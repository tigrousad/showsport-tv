##################################
# Author: tigrousad #########
# File: showsport-tv.py ##########
##################################
# dedicated to SamSamSam #
##################################


def init():
    return {
        'protocol': 'showsport-tv',
        'hostname': 'showsport-tv.com',
        'regex': '',
        'httpDelay': [1, 3], 
    }
 
def url(url):
    setPageLoadTimeout( 90 )
    
    if '/event/' in url:
        parse_streampage(url)
    else:
        parse_schedulepage(url)
 
def parse_streampage(url):
    load(url)
     
    try:
        # Raw Asset
        asset_elem = find('div[id=timezone] span')
        store('raw_asset', asset_elem.text[17:-12])     
        
        # Find and load embedded local page
       
        # Try to load embedded local page
        try:
            host_url_elem = find('div[id=middle_content] > iframe')
            linking_site_url = queryAttrib('src', host_url_elem)
            load(linking_site_url)
        
            # Linking Site URL
            store('linking_site_url', linking_site_url)
            
            # Hosting Site url
            live_url_elem = find('body iframe')
            live_url = queryAttrib('src', live_url_elem)
            store('hosting_site_url', live_url)
        except:
            # No channel
            store('linking_site_url', url)
            store('hosting_site_url', 'http://showsport-tv.com/nochannel.php')     


        # Raw Category
        store('raw_category', None)
        
        # Raw Subcategory
        store('raw_subcategory', None)
        
        # Type
        store('type', 'linking_site_url')
     
        flush()
    except Exception as genErr:
        err( genErr )
 
def parse_schedulepage(url):
    load(url)
    try:
        # Listings
        listings = findAll('div.listmatch > ul > li')
        for listing in listings:
            try:
                # URL
                store('url', url)
                
                
                # Raw Asset
                home_elem = find('div.team.column span', listing)
                away_elem = find('div.away.column', listing)
                store('raw_asset', home_elem.text + ' ' + away_elem.text)
         
                # Linking Page URL
                link_elem = find('div.live_btn a', listing)
                linking_site_url = queryAttrib('href', link_elem)
                store('linking_site_url', linking_site_url)
         
                # Raw Category
                cat_elem = find('div.leaguelogo img', listing)
                cat_img = queryAttrib('src', cat_elem)
                store('raw_category', cat_img[35:-4])
                
                # Raw Subcategory - N/A
                store('raw_subcategory', None)
         
                # Type (Will always be this)
                store('type', 'search_result')
     
                # Create a new job for your new found url
                store('new_job', linking_site_url)
         
                flush()

            except Exception as genErr:
                err( genErr )
                break
    except Exception as genErr:
        err( genErr )
